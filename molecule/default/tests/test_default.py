import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_directories(host):
    dirs = [
            "/etc/ma1sd",
    ]
    for dir in dirs:
        d = host.file(dir)
        assert d.is_directory
        assert d.exists


def test_files(host):
    files = [
            "/etc/systemd/system/ma1sd.service",
            "/etc/ma1sd/ma1sd.yml"
    ]
    for file in files:
        f = host.file(file)
        assert f.exists
        assert f.is_file


def test_service(host):
    s = host.service("ma1sd")
    assert s.is_running
    assert s.is_enabled


def test_socket(host):
    sockets = [
        "tcp://0.0.0.0:8090"
    ]
    for socket in sockets:
        s = host.socket(socket)
        assert s.is_listening
