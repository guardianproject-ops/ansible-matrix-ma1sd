## Role Variables

* `ma1sd_version`: `"2.4.0"` - the ma1sd version to install, must be a string!



* `ma1sd_checksum`: `"sha256:57029e55a838d745c0ed3f01f22ab4f1977f59f411b37248c604f8e24932076b"` - sha256 hash of the version deb's file



* `ma1sd_hostname`: `` - your homeserver domain



* `ma1sd_homeserver_internal_uri`: `` - (optional) The internal ip address/hostname to resolve your homeserver domain to.



* `ma1sd_synapse_db_endpoint`: `` - synapse postgres db hostname



* `ma1sd_synapse_db_name`: `` - synapse db name



* `ma1sd_synapse_db_username`: `` - synapse db username



* `ma1sd_synapse_db_password`: `` - synapse db password



* `ma1sd_email_enabled`: `false` - whether to enable SMTP for 3PID verification



* `ma1sd_email_port`: `587` - SMTP server port



* `ma1sd_email_from_name`: `` - the name of the sender



* `ma1sd_email_from_address`: `` - the email address of the sender



* `ma1sd_email_host`: `` - SMTP server hostname



* `ma1sd_email_login`: `` - SMTP server username



* `ma1sd_email_password`: `` - SMTP server password



* `ma1sd_email_tls_setting`: `2` - SMTP server tls setting.  0 = no STARTLS, 1 = try, 2 = force, 3 = TLS/SSL



* `ma1sd_tos_enabled`: `false` - whether to enable TOS acceptance.



* `ma1sd_tos_version`: `"1.0"` - the tos version, must be a string!



* `ma1sd_tos_name`: `` - user readable name of the tos



* `ma1sd_tos_url`: `` - link to the tos terms


